package com.sda.travelagency.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sda.travelagency.model.Airport;

@Repository
public interface AirportRepository extends JpaRepository <Airport, Integer> {

	
	List<Airport> findAll();
}
