package com.sda.travelagency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sda.travelagency.model.Continent;

@Repository
public interface ContinentRepository extends JpaRepository<Continent, Integer> {

}
