package com.sda.travelagency.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sda.travelagency.model.Country;

@Repository
public interface CountryRepository extends JpaRepository <Country, Integer> {

	
	public List<Country>findAllByOrderByCountryNameAsc();
}
