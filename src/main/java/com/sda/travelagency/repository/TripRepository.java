package com.sda.travelagency.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sda.travelagency.model.Trip;

@Repository
public interface TripRepository extends JpaRepository<Trip, Integer> {
	
	public List<Trip> findAll();

	public List<Trip> findByArrivalCityCountryIdCountry(Integer idCountry);

	public List<Trip> findByArrivalCityIdCity(Integer idCity);
	
	public List<Trip> findByArrivalCityCountryContinentIdContinent(Integer idContinent);
	
	public List<Trip> findByArrivalHotelCityIdCity(Integer idHotel);
	
	
	public List<Trip> findByArrivalCityCityNameContainsOrArrivalCityCountryCountryNameContains(String text, String text2);
	
	@Query("select trip from Trip trip where trip.arrivalCity.cityName like '%' || :text || '%'"
			+ " or trip.arrivalCity.country.countryName like '%' || :text || '%'"
			+ " or trip.arrivalAirport.airportName like '%' || :text || '%'"
			+ " or trip.arrivalHotel.hotelName like '%' || :text || '%'"
			+ " or trip.arrivalCity.country.continent.continentName like '%' || :text || '%'")
	public List<Trip> search(String text);
	

}
