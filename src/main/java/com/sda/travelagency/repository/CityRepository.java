package com.sda.travelagency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sda.travelagency.model.City;

@Repository
public interface CityRepository extends JpaRepository<City, Integer> {

}
