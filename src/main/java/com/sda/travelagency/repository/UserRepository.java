package com.sda.travelagency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sda.travelagency.model.User;

@Repository
public interface UserRepository extends JpaRepository <User, Integer> {


	User findByEmail(String name);

}
