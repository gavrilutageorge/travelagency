package com.sda.travelagency.service;

import java.util.List;

import com.sda.travelagency.model.Airport;

public interface AirportService {
	
	public void addAirport(Airport airport);
	
	public List<Airport> getAllAirports();

}
