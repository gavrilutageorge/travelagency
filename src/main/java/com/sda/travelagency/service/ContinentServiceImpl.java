package com.sda.travelagency.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sda.travelagency.model.Continent;
import com.sda.travelagency.repository.ContinentRepository;

@Service
public class ContinentServiceImpl implements ContinentService {

	@Autowired
	private ContinentRepository continentRepository;

	@Override
	public void addContinent(Continent continent) {
		continentRepository.save(continent);

	}

}
 