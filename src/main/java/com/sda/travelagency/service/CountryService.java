package com.sda.travelagency.service;

import java.util.List;

import com.sda.travelagency.model.Country;

public interface CountryService {

	public void addCountry(Country country);
	

	public List<Country> findAll();
	
	public Country findById(int theId);
	
	public void save(Country theCountry);
	
	public void deleteById(int theId);


	
}
