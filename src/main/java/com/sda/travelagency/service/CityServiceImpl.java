package com.sda.travelagency.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sda.travelagency.model.Airport;
import com.sda.travelagency.model.City;
import com.sda.travelagency.repository.CityRepository;

@Service
public class CityServiceImpl implements CityService {

	@Autowired
	private CityRepository cityRepository;

	@Override
	public void addCity(City city) {
		cityRepository.save(city);

	}

	@Override
	public List<City> getAllcities() {

		List<City> cityList = cityRepository.findAll();
		return cityList;
	}

}
