package com.sda.travelagency.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sda.travelagency.model.Hotel;
import com.sda.travelagency.model.Purchase;
import com.sda.travelagency.repository.PurchaseRepository;

@Service
public class PurchaseServiceImpl implements PurchaseService {

	@Autowired
	private PurchaseRepository purchaseRepository;

	@Override
	public Purchase save(Purchase purchase) {

		return purchaseRepository.save(purchase);
	}

	@Override
	public List<Purchase> findAll() {
		return purchaseRepository.findAll();
	}

	@Override
	public Purchase findById(Integer id) {
			return purchaseRepository.findById(id).orElse(null);
	}

	@Override
	public void deleteById(int theId) {
		purchaseRepository.deleteById(theId);
	}

	@Override
	public void savePurchase(int thePurchase) {
		purchaseRepository.saveAndFlush(thePurchase);
		
	}

	@Override
	public void savePurchase(Purchase thePurchase) {
		purchaseRepository.saveAndFlush(thePurchase);		
	}




}
