package com.sda.travelagency.service;

import java.util.List;

import com.sda.travelagency.model.City;

public interface CityService {
	
	public void addCity(City city);
	
	public List<City> getAllcities();

}
