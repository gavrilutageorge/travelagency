package com.sda.travelagency.service;

import java.util.List;

import com.sda.travelagency.model.Airport;
import com.sda.travelagency.model.Trip;

public interface TripService {
	public List<Trip> findAll();
	

	public void addTrip(Trip trip);

	public Trip findById(int theId);

	public void save(Trip theTrip);

	public void deleteById(int theId);
	
	public void addTripTry(Trip trip, Airport airport);

	public List <Trip> findByCountry(Integer idCountry);
	
	public List<Trip> findByCity(Integer idCity);
	
	public List<Trip> findByContinent(Integer idContinent);
	
	public List<Trip> findByHotel(Integer idHotel);
	
	public int getTotalAmount(Integer idTrip);
	
	public List<Trip> search(String text);


}
