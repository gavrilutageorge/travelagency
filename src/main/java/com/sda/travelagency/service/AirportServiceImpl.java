package com.sda.travelagency.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sda.travelagency.model.Airport;
import com.sda.travelagency.repository.AirportRepository;

@Service
public class AirportServiceImpl implements AirportService {

	@Autowired
	private AirportRepository airportRepository;
	
	@Override
	public void addAirport(Airport airport) {
		// TODO Auto-generated method stub
		airportRepository.save(airport);
	}

	@Override
	public List<Airport> getAllAirports() {
		List<Airport> airportList = airportRepository.findAll();
		System.out.println("*****************************************");
		System.out.println("Airport list" + airportList);
		System.out.println("*****************************************");

		return airportList;
	}

}
