package com.sda.travelagency.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sda.travelagency.model.Airport;
import com.sda.travelagency.model.Trip;
import com.sda.travelagency.repository.TripRepository;

@Service
public class TripServiceImpl implements TripService {

	@Autowired
	private TripRepository tripRepository;

	@Override
	public void addTrip(Trip trip) {
		tripRepository.save(trip);

	}

	@Override
	public Trip findById(int theId) {
		return tripRepository.findById(theId).orElse(null);
	}

	@Override
	public void save(Trip theTrip) {
		tripRepository.save(theTrip);

	}

	@Override
	public void deleteById(int theId) {
		tripRepository.deleteById(theId);
	}

	@Override
	public List<Trip> findAll() {
		return tripRepository.findAll();
	}

	@Override
	public void addTripTry(Trip trip, Airport airport) {

		trip.setArrivalAirport(airport);
		tripRepository.save(trip);

	}

	@Override
	public List <Trip> findByCountry(Integer idCountry) {
		return tripRepository.findByArrivalCityCountryIdCountry(idCountry);

	}

	@Override
	public List<Trip> findByCity(Integer idCity) {
		return tripRepository.findByArrivalCityIdCity(idCity);
	}

	@Override
	public List<Trip> findByContinent(Integer idContinent) {
		return tripRepository.findByArrivalCityCountryContinentIdContinent(idContinent);
	}

	@Override
	public List<Trip> findByHotel(Integer idHotel) {
		return tripRepository.findByArrivalHotelCityIdCity(idHotel);
	}

	@Override
	public int getTotalAmount(Integer idTrip) {
		Trip trip = findById(idTrip);
		int result = trip.getNumberOfDays() * trip.getPriceForAdult() + 
				trip.getNumberOfDays() * trip.getPriceForChildren();
		
		return result; 
	}

	@Override
	public List<Trip> search(String text) {
		return tripRepository.search(text);
	}



}
