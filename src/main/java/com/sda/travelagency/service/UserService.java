package com.sda.travelagency.service;

import com.sda.travelagency.model.User;

public interface UserService {

	void save(User user);

	User findByEmail(String name);
}
