package com.sda.travelagency.service;

import com.sda.travelagency.model.Continent;

public interface ContinentService {

	
	public void addContinent(Continent continent);
	
	
}
