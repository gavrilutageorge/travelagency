package com.sda.travelagency.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sda.travelagency.model.Country;
import com.sda.travelagency.repository.CountryRepository;

@Service
public class CountryServiceImpl implements CountryService {

	@Autowired
	private CountryRepository countryRepository;

	@Override
	public void addCountry(Country country) {
		countryRepository.save(country);
	}

	@Override
	public List<Country> findAll() {
		return countryRepository.findAllByOrderByCountryNameAsc();
	}

	@Override
	public Country findById(int theId) {
	return countryRepository.findById(theId).orElse(null);
	}

	@Override
	public void save(Country theCountry) {
		countryRepository.save(theCountry);

	}
 
	@Override
	public void deleteById(int theId) {
		countryRepository.deleteById(theId);

	}

}
