package com.sda.travelagency.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sda.travelagency.model.City;
import com.sda.travelagency.model.Hotel;
import com.sda.travelagency.repository.HotelRepository;

@Service
public class HotelServiceImpl implements HotelService {

	@Autowired
	private HotelRepository hotelRepository;
	
	@Override
	public void addHotel(Hotel hotel) {
		// TODO Auto-generated method stub
		hotelRepository.save(hotel);
	}

	@Override
	public List<Hotel> getAllHotels() {
		List<Hotel> hotelList = hotelRepository.findAll();
		System.out.println("*****************************************");
		System.out.println("hotel " + hotelList);
		System.out.println("*****************************************");
		return hotelList;
	}

}
 