package com.sda.travelagency.service;

import java.util.List;

import com.sda.travelagency.model.Hotel;

public interface HotelService {

	public void addHotel(Hotel hotel);

	public List<Hotel> getAllHotels();

}
