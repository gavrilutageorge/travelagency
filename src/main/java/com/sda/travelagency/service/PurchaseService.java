package com.sda.travelagency.service;

import java.util.List;

import com.sda.travelagency.model.Purchase;

public interface PurchaseService {

	Purchase save(Purchase purchase);

	List<Purchase> findAll();

	Purchase findById(Integer id);

	void deleteById(int theId);

	void savePurchase(Purchase thePurchase);

	void savePurchase(int thePurchase);
}
