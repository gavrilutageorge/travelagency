package com.sda.travelagency;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	// parola de admin e luata din application.properties,
	// proprietatea cu cheia security.adminPassword
	@Value("${security.adminPassword}")
	private String adminPassword;

	// datasource este implementat deja de Spring
	@Autowired
	private DataSource dataSource;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			// definim accesul pe diverse resurse / grupuri de resurse
			// pentru diverse roluri, pentru toti userii autentificati
			// sau pentru toata lumea
			.authorizeRequests()
				.antMatchers("/public/**").permitAll()
				.antMatchers("/").permitAll()
				.antMatchers("/**").permitAll()
				.antMatchers("/main.css").permitAll()
				.anyRequest().permitAll()
				.and()
			// toata lumea are acces pe form-ul de login
			.formLogin()
				.permitAll();
	}
	
	@Autowired
	protected void configureGlobal(AuthenticationManagerBuilder auth) 
			  throws Exception {
		// System.out.println("\n\nADMIN PASSWORD: " + adminPassword + "\n\n");
	    auth
	    	// useri, parole si roluri hardcoded in aplicatie
	    	.inMemoryAuthentication()
	    		.withUser("admin")
	    			.password(passwordEncoder().encode(adminPassword))
	    			.roles("ADMIN")
	    			.and()
	    		.withUser("guest")
	    			.password(passwordEncoder().encode("guest"))
	    			.roles("GUEST")
	    			.and()
	    			    		
	    		.and()
	    	// useri si parole in baza de date
	    	.jdbcAuthentication()
	    		.dataSource(dataSource)
    			.usersByUsernameQuery("select email,password,1 "
    					+ "from users "
    					+ "where email = ?")
    			.authoritiesByUsernameQuery("select email,if(admin = 1, 'ADMIN', 'GUEST') "
    					+ "from users "
    					+ "where email = ?");
    					
	}
	
	// encoder pentru parole
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(); 
	}
	
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
	    return super.authenticationManagerBean();
	}
}
