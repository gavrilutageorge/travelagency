package com.sda.travelagency.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Purchase {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idPurchase;
	private String firstName;
	private String lastName;
	private int totalAmount;
	
	@ManyToOne
	@JoinColumn(name = "idTrip")
	private Trip trip;
	
	
	public Purchase(){
		
	}

	public Purchase(int idPurchase, String firstName, String lastName, int totalAmount, Trip trip) {
		super();
		this.idPurchase = idPurchase;
		this.firstName = firstName;
		this.lastName = lastName;
		this.totalAmount = totalAmount;
		this.trip = trip;
	}

	public int getIdPurchase() {
		return idPurchase;
	}

	public void setIdPurchase(int idPurchase) {
		this.idPurchase = idPurchase;
	}



	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}


	public Trip getTrip() {
		return trip;
	}

	public void setTrip(Trip trip) {
		this.trip = trip;
	}

	@Override
	public String toString() {
		return "Purchase [idPurchase=" + idPurchase + ", tripDetails=" + firstName + ", personDetails="
				+ lastName + ", totalAmount=" + totalAmount + ", trip=" + trip + "]";
	}

}
