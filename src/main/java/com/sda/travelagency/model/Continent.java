package com.sda.travelagency.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Continent {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idContinent;
	
	private String continentName;
	


	public int getIdContinent() {
		return idContinent;
	}



	public void setIdContinent(int idContinent) {
		this.idContinent = idContinent;
	}



	public String getContinentName() {
		return continentName;
	}

	public void setContinentName(String continentName) {
		this.continentName = continentName;
	}
	@Override
	public String toString() {
		return "Continets [idContinent=" + idContinent + ", continentName=" + continentName + "]";
	}
}
