package com.sda.travelagency.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class City {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idCity;
	private String cityName;
	
	@ManyToOne
	@JoinColumn(name = "idCountry")
	private Country country;

	public int getIdCity() {
		return idCity;
	} 

	public void setIdCity(int idCity) {
		this.idCity = idCity;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "City [idCity=" + idCity + ", cityName=" + cityName + ", country=" + country + "]";
	}
}
