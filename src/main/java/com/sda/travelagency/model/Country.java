package com.sda.travelagency.model;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Country {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idCountry;

	private String countryName;
	
	@ManyToOne
	@JoinColumn(name = "idContinent")
	private Continent continent;


	
	public Country() {
		
	}

	public Country(int idCountry, String countryName, Continent continent) {
		
		this.idCountry = idCountry;
		this.countryName = countryName; 
		this.continent = continent;
	}
	public int getIdCountry() {
		return idCountry;
	}
	public void setIdCountry(int idCountry) { 
		this.idCountry = idCountry;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Continent getContinent() {
		return continent;
	}

	public void setContinent(Continent continent) {
		this.continent = continent;
	}

	@Override
	public String toString() {
		return "Country [idCountry=" + idCountry + ", countryName=" + countryName + ", continent=" + continent + "]";
	}

}
