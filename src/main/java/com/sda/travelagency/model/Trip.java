package com.sda.travelagency.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Trip {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idTrip;
	private int priceForAdult;
	private int priceForChildren;
	private int numberOfBeds;
	private int numberOfBedsForChildren;
	private int numberOfDays;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date departDate;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date returnDate;
	private String imageUrl;



	@ManyToOne
	@JoinColumn(name = "idDepartCity")
	private City departCity;

	@ManyToOne
	@JoinColumn(name = "idArrivalCity")
	private City arrivalCity;

	@ManyToOne 
	@JoinColumn(name = "idDepartAirport")
	private Airport departAirport;

	@ManyToOne
	@JoinColumn(name = "idArrivalAirport")
	private Airport arrivalAirport;

	@ManyToOne 
	@JoinColumn(name = "idArrivalHotel")
	private Hotel arrivalHotel;




	public Trip() {

	}

	public Trip(int idTrip, int priceForAdult, int priceForChildren, int numberOfBeds, int numberOfBedsForChildren,
			int numberOfDays, Date departDate, Date returnDate, String tripName, City departCity, City arrivalCity,
			Airport departAirport, Airport arrivalAirport, Hotel arrivalHotel) {
		super();
		this.idTrip = idTrip;
		this.priceForAdult = priceForAdult;
		this.priceForChildren = priceForChildren;
		this.numberOfBeds = numberOfBeds;
		this.numberOfBedsForChildren = numberOfBedsForChildren;
		this.numberOfDays = numberOfDays;
		this.departDate = departDate;
		this.returnDate = returnDate;
		this.departCity = departCity;
		this.arrivalCity = arrivalCity;
		this.departAirport = departAirport;
		this.arrivalAirport = arrivalAirport;
		this.arrivalHotel = arrivalHotel;

	}

	public int getIdTrip() {
		return idTrip;
	}

	public void setIdTrip(int idTrip) {
		this.idTrip = idTrip;
	}

	public int getPriceForAdult() {
		return priceForAdult;
	}

	public void setPriceForAdult(int priceForAdult) {
		this.priceForAdult = priceForAdult;
	}

	public int getPriceForChildren() {
		return priceForChildren;
	}

	public void setPriceForChildren(int priceForChildren) {
		this.priceForChildren = priceForChildren;
	}

	public int getNumberOfBeds() {
		return numberOfBeds;
	}

	public void setNumberOfBeds(int numberOfBeds) {
		this.numberOfBeds = numberOfBeds;
	}

	public int getNumberOfBedsForChildren() {
		return numberOfBedsForChildren;
	}

	public void setNumberOfBedsForChildren(int numberOfBedsForChildren) {
		this.numberOfBedsForChildren = numberOfBedsForChildren;
	}

	public int getNumberOfDays() {
		return numberOfDays;
	}

	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}

	public Date getDepartDate() {
		return departDate;
	}

	public void setDepartDate(Date departDate) {
		this.departDate = departDate;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public City getDepartCity() {
		return departCity;
	}

	public void setDepartCity(City departCity) {
		this.departCity = departCity;
	}

	public City getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(City arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public Airport getDepartAirport() {
		return departAirport;
	}

	public void setDepartAirport(Airport departAirport) {
		this.departAirport = departAirport;
	}

	public Airport getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(Airport arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public Hotel getArrivalHotel() {
		return arrivalHotel;
	}

	public void setArrivalHotel(Hotel arrivalHotel) {
		this.arrivalHotel = arrivalHotel;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Override
	public String toString() {
		return "Trip [idTrip=" + idTrip + ", priceForAdult=" + priceForAdult + ", priceForChildren=" + priceForChildren
				+ ", numberOfBeds=" + numberOfBeds + ", numberOfBedsForChildren=" + numberOfBedsForChildren
				+ ", numberOfDays=" + numberOfDays + ", departDate=" + departDate + ", returnDate=" + returnDate
				+ ", departCity=" + departCity + ", arrivalCity=" + arrivalCity + ", departAirport=" + departAirport
				+ ", arrivalAirport=" + arrivalAirport + ", arrivalHotel=" + arrivalHotel + "]";
	}

}
