package com.sda.travelagency.model;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Airport {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idAirport;
	private String airportName;
	private String airportAddress;
	
	@ManyToOne
	@JoinColumn(name = "idCity")
	private City city;


	public Airport() {
		
	}

	public Airport(int idAirport, String airportName, String airportAddress, City city) {
		super();
		this.idAirport = idAirport;
		this.airportName = airportName;
		this.airportAddress = airportAddress;
		this.city = city;
	}

	public int getIdAirport() {
		return idAirport;
	}

	public void setIdAirport(int idAirport) {
		this.idAirport = idAirport;
	}

	public String getAirportName() {
		return airportName;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	public String getAirportAddress() {
		return airportAddress;
	}

	public void setAirportAddress(String airportAddress) {
		this.airportAddress = airportAddress;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "airportName=" + airportName + ", airportAddress=" + airportAddress + " ";
	}

//	@Override
//	public String toString() {
//		return "Airport [idAirport=" + idAirport + ", airportName=" + airportName + ", airportAddress=" + airportAddress
//				+ ", city=" + city + "]";
//	}
	
	

}
