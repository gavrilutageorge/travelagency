package com.sda.travelagency.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sda.travelagency.model.City;
import com.sda.travelagency.model.Continent;
import com.sda.travelagency.model.Country;
import com.sda.travelagency.service.CountryService;

@Controller
@RequestMapping("/country")
public class CountryController {

	@Autowired
	private CountryService countryService;

	public CountryController(CountryService theCountryService) {
		countryService = theCountryService;
	} 

	@GetMapping("/list")
	public String listCountries(Model theModel) {

		// get countries from db
		List<Country> theCountry = countryService.findAll();

		theModel.addAttribute("country", theCountry);
		return "country/crud";
	}

	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model theModel) {
		Country theCountry = new Country();

		theModel.addAttribute("country", theCountry); 
		return "country/form";
	}
	
	
	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("countryId") int theId, Model theModel) {
		
		Country theCountry = countryService.findById(theId);
		
		theModel.addAttribute("country", theCountry);
		
		return "country/form";
		
	}


	@PostMapping("/save")
	public String saveCountry(@ModelAttribute("country") Country theCountry) {
		countryService.save(theCountry);
		return "redirect:/country/list";

	}
	
	@GetMapping("/delete")
	public String delete(@RequestParam("countryId") int theId) {
		countryService.deleteById(theId); 
		return "redirect:/country/list";
	}
	


	@PostMapping("/addCountry")
	public String addCountryToDb(Model model, @ModelAttribute Country country) {

		countryService.addCountry(country);
		return "continent";
	}
	

	@ModelAttribute
	public Country getCountry() {
		return new Country();
	}


	
}
