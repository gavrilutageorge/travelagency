package com.sda.travelagency.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sda.travelagency.model.Airport;
import com.sda.travelagency.model.City;
import com.sda.travelagency.model.Hotel;
import com.sda.travelagency.model.Purchase;
import com.sda.travelagency.model.Trip;
import com.sda.travelagency.service.AirportService;
import com.sda.travelagency.service.CityService;
import com.sda.travelagency.service.HotelService;
import com.sda.travelagency.service.TripService;

@Controller
@RequestMapping("/trip")
public class TripController {

	@Autowired
	private TripService tripService;

	@Autowired
	private AirportService airportService;

	@Autowired
	private CityService cityService;

	@Autowired
	private HotelService hotelService;

	public TripController(TripService theTripService) {
		tripService = theTripService;
	}

	@PostMapping("/addTrip")
	public String addCountryToDb(Model model, @ModelAttribute Trip trip) {

		tripService.addTrip(trip);
		return "trip/booking";
	}

	@GetMapping("/list")
	public String listOfTrips(Model theModel) {

		// get countries from db
		List<Trip> listOfTrips = tripService.findAll();

		theModel.addAttribute("allTrips", listOfTrips);
		return "trip/booking";
	}

	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model theModel) {
		Trip theTrip = new Trip();

		theModel.addAttribute("trip", theTrip);
		List<Airport> airportList = airportService.getAllAirports();
		theModel.addAttribute("airports", airportList);
		List<City> cityList = cityService.getAllcities();
		theModel.addAttribute("cities", cityList);
		List<Hotel> hoteList = hotelService.getAllHotels();
		theModel.addAttribute("hotels", hoteList);

		return "trip/admintrip";

	}

	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("tripId") int theId, Model theModel) {

		Trip theTrip = tripService.findById(theId);

		theModel.addAttribute("trip", theTrip);
		List<Airport> airportList = airportService.getAllAirports();
		theModel.addAttribute("airports", airportList);
		List<City> cityList = cityService.getAllcities();
		theModel.addAttribute("cities", cityList);
		List<Hotel> hoteList = hotelService.getAllHotels();
		theModel.addAttribute("hotels", hoteList);

		return "trip/admintrip";

	}

	@PostMapping("/savetry")
	public String saveTrip(@ModelAttribute("trip") Trip theTrip) {
		System.out.println("----------------------------");
		System.out.println("airport " + theTrip);
		System.out.println("----------------------------");

		tripService.save(theTrip);

		return "redirect:/trip/list";

	}

	@GetMapping("/delete")
	public String delete(@RequestParam("tripId") int theId) {
		tripService.deleteById(theId);
		return "redirect:/trip/list";
	}

	@GetMapping("/airportsList")
	public String generateAirportsList(Model model) {
		List<Airport> airportList = airportService.getAllAirports();
		model.addAttribute("airports", airportList);

		return "trip/booking";

	}

	@ModelAttribute
	public Trip getTrip() {
		return new Trip();
	}

	@ModelAttribute
	public Airport getAirport() {
		return new Airport();
	}

	@GetMapping("/allSearch")
	public String searchTrips(Model theModel, @RequestParam(value = "country", required = false) Integer idCountry,
			@RequestParam(value = "city", required = false) Integer idCity,
			@RequestParam(value = "continent", required = false) Integer idContinent,
			@RequestParam(value = "hotel", required = false) Integer idHotel) {
		List<Trip> matchingTrips;
		// get countries from db
		if (idCountry != null) {
			matchingTrips = tripService.findByCountry(idCountry);
		} else {
			matchingTrips = tripService.findAll();
		}

		if (idCity != null) {
			matchingTrips = tripService.findByCity(idCity);
		} else {
			matchingTrips = tripService.findAll();
		}

		if (idContinent != null) {
			matchingTrips = tripService.findByContinent(idContinent);
		} else {
			matchingTrips = tripService.findAll();
		}

		if (idHotel != null) {
			matchingTrips = tripService.findByHotel(idHotel);
		} else {
			matchingTrips = tripService.findAll();
		}
		theModel.addAttribute("allTrips", matchingTrips);
		return "trip/booking";
	}

	
	@GetMapping("/search")
	public String search(Model model, @RequestParam("text") String text) {
		List<Trip> matchingTrips= tripService.search(text);
		model.addAttribute("allTrips", matchingTrips);
		return "trip/booking";
	}
	@GetMapping("/view/{id}")
	public String yourTrip(Model theModel, @PathVariable("id") Integer id) {

		Trip trip = tripService.findById(id);
		Purchase purchase = new Purchase();
		purchase.setTrip(trip);

		theModel.addAttribute("trip", trip);
		theModel.addAttribute("purchase", purchase);
		return "trip/trip";
	}

}
