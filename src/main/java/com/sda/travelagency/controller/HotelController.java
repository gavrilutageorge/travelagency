package com.sda.travelagency.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.sda.travelagency.model.Hotel;
import com.sda.travelagency.service.HotelService;

@Controller
public class HotelController {

	
	@Autowired
	private HotelService hotelServiceService;
	
	@PostMapping("/addHotel")
	public String addHotelToDb(Model model,  @ModelAttribute Hotel hotel) {
		
		hotelServiceService.addHotel(hotel);
		return "continent";
	}
	
//	@GetMapping("/addHotel")
//	public String hotel() {
//		return "continent";
//	}
}
