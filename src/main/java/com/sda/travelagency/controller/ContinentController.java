package com.sda.travelagency.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.sda.travelagency.model.Continent;
import com.sda.travelagency.service.ContinentService;

@Controller
public class ContinentController {

	@Autowired
	private ContinentService continentService;

	@PostMapping("/addContinent")
	public String addContinentToDb(Model model, @ModelAttribute Continent continent) {

		continentService.addContinent(continent);
		return "continent";
	}

	@GetMapping("/continent")
	public String continent() {
		return "continent";
	}

}
