package com.sda.travelagency.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sda.travelagency.model.Trip;
import com.sda.travelagency.model.User;
import com.sda.travelagency.service.TripService;
import com.sda.travelagency.service.UserService;

@Controller
public class WelcomeController {

	@Autowired
	private TripService tripService;

	@Autowired
	private UserService userService;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private AuthenticationManager authenticationManager;

	@PostMapping("/registerSubmit")
	public String registerSubmit(@Valid @ModelAttribute("user") User user, BindingResult bindingResult) {

		if (bindingResult.hasErrors())
			return "welcome";

		user.setAdmin(false);
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		userService.save(user);
		return "redirect:/";
	}

	@PostMapping("/loginSubmit")
	public String loginSubmit(String email, String password, HttpServletRequest req) {


		UsernamePasswordAuthenticationToken authReq
		 = new UsernamePasswordAuthenticationToken(email, password);
		Authentication auth = authenticationManager.authenticate(authReq);
		SecurityContext sc = SecurityContextHolder.getContext();
		sc.setAuthentication(auth);	
	    HttpSession session = req.getSession(true);
	    session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, sc);
		
		return "redirect:/";
	}

	@PostMapping("/logoutSubmit")
	public String logoutSumbit(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null){    
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	        System.out.println("logout ok");
	    }
	    return "redirect:/";
	}
	
	
	@RequestMapping("/")
	public String showView(Model model, Principal principal) {
		List<Trip> listOfTrips = tripService.findAll();
//  	User user = userService.findByEmail(principal.getName());
		User user = new User();
		model.addAttribute("user", user);

		model.addAttribute("tripList", listOfTrips);
		return "welcome";
	}
}
