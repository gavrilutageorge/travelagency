package com.sda.travelagency.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.sda.travelagency.model.City;
import com.sda.travelagency.service.CityService;

@Controller
public class CityController {
	@Autowired
	private CityService cityService;

	@PostMapping("/addCity")
	public String addCityToDb(Model model, @ModelAttribute City city) {

		cityService.addCity(city);
		return "continent";
	}


}
