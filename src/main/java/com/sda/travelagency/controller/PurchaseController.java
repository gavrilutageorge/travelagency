package com.sda.travelagency.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sda.travelagency.model.Country;
import com.sda.travelagency.model.Purchase;
import com.sda.travelagency.service.PurchaseService;
import com.sda.travelagency.service.TripService;

@Controller
@RequestMapping("/purchase")
public class PurchaseController {
	
	@Autowired
	private TripService tripService;
	
	@Autowired
	private PurchaseService purchaseService;
	

	
	@GetMapping("/newPurchase")
	public String newPurchase(Integer idTrip ,@ModelAttribute("purchase") Purchase purchase){
		System.out.println("------------------");
		System.out.println("id Trip" + idTrip);
		System.out.println("------------------");
		
		purchase.setTrip(tripService.findById(idTrip));
		purchase.setTotalAmount(tripService.getTotalAmount(idTrip));
		purchase = purchaseService.save(purchase);
		
		return "redirect:/purchase/view/" + purchase.getIdPurchase();
		
	}
	
	@GetMapping("/list")
	public String purchaseList(Model theModel) {

		List<Purchase> purchaseList = purchaseService.findAll();

		theModel.addAttribute("allPurchase", purchaseList);
		return "/trip/purchaselist";
		
	}
	@GetMapping("/view/{id}")
	public String purchaseView(Model theModel, @PathVariable("id") Integer id) {

		Purchase purchase = purchaseService.findById(id); 

		theModel.addAttribute("purchase", purchase);
		return "/trip/purchase";
		
	}
	
	@GetMapping("/delete")
	public String delete(@RequestParam("purchaseId") int theId) {
		purchaseService.deleteById(theId);
		return "redirect:/purchase/list";
	}
	
	
	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("purchaseId") int theId, Model theModel) {

		Purchase thePurchase = purchaseService.findById(theId);

		theModel.addAttribute("purchase", thePurchase);
//		List<Purchase> purcListList = purchaseService.findAll();
//		theModel.addAttribute("purchase", purcListList);


		return "country/purchaseform";

	}
	
	@PostMapping("/save")
	public String savePurchase(@ModelAttribute("purchaseId") Purchase thePurchase) {
		purchaseService.savePurchase(thePurchase);
		return "redirect:/purchase/list";
 
	}
}
