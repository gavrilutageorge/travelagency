package com.sda.travelagency.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.sda.travelagency.model.Airport;
import com.sda.travelagency.service.AirportService;

@Controller
public class AirportController {

	@Autowired
	private AirportService airportService;

	@PostMapping("/addAirport")
	public String addAirportToDb(Model model, @ModelAttribute Airport airport) {

		airportService.addAirport(airport);
		return "continent";
	}


}
